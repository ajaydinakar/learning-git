~~~
git init             //creates a git repository with working branch as master
~~~
~~~
git status           //know the status of the git repo
~~~
~~~
git brach new        //creates a branch called new
~~~
~~~

git checkout new     //changes working branch from master to new
~~~
~~~
git add file.txt     // adds file for staging 
~~~
~~~
git add -A          //adds every unstaged file
~~~
~~~
git reset file.txt    //remove the file.txt from staging
~~~
~~~
git reset              //remove all files from staging
~~~
~~~
git commit -m "adding file.txt to repo"    //saves the file in the repo
~~~
~~~
git diff              //shows changes we made to the code
~~~
~~~
git clone "url" .       //clones the remote repository from url location to (.) current directory
~~~
~~~
git add remote ""      //adds local git to remote
~~~
~~~
git pull origin master  //pulls changes from origin repo with branch name master
~~~
~~~
git push origin master   //pushes the code from local repo to remote repo with name origin and branch - master
~~~
~~~
git branch               //gives the branch currently working on
~~~
~~~
git branch -a             //shows all branches
~~~
~~~
git branch new           //creates a branch named new
~~~
~~~
git checkout new         //moves to branch named new
~~~
~~~
git push -u origin new   //push to specific branch
~~~
~~~
git pull                  //pull to local repo using pull
~~~
~~~
git merge new            //merge the new branch to master
~~~
~~~
git branch --merged      //info about merged branches
~~~
~~~
git branch -d new        // delete the branch
~~~
~~~
git push origin --delete calc-divide //delete the branch in remote repo
~~~
~~~
git stash         //save current status at stack and dont commit again
~~~
~~~
git stash list     //list stashed things
~~~
~~~
git stash apply stash{1} //retrive the saved changes
~~~
~~~
git stash pop            //retrive the saved change at top of the stash
~~~
~~~